import { Component } from '@angular/core';

import { Input } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent {

  /**
   * コンストラクタ
   * @memberof ModalComponent
   */
  constructor() {}

  /**
   * 親コンポーネントから受け取るデータ(文字列)をセットするプロパティ
   *
   * @type string[]
   * @memberof AppComponent
   */
  @Input() pictureList: string[] = [];
}
