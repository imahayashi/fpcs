import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PixabayService {

  private httpOptions: any = {
    // ヘッダ情報
    headers: new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
    }),
  };
  private apiUrl: string = environment.apiUrl;

  constructor(private http: HttpClient) { }

  public get(formData: object): Promise<object> {
    return this.http.post(this.apiUrl, formData, this.httpOptions)
      .toPromise()
      .then((res) => {
        const response: object = res;
        return response;
      })
      .catch(this.errorHandler);
  }

  private errorHandler(err) {
    console.log('Error occured.', err);
    return Promise.reject(err.message || err);
  }
}
