import { Component } from '@angular/core';
import { ViewEncapsulation } from "@angular/core";
import { Output, EventEmitter } from '@angular/core';
import { PixabayService } from './services/pixabay.service';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {

  public title: string = 'pixabay検索サイト PCS';
  @Output() searchEvent = new EventEmitter();
  public searched: boolean = false;
  public searching: boolean = false;
  public formData: object;
  public total: number = 0;
  public pictureList = [];
  public searchData = {
    typePhoto: false,
    typeIllustration: false,
    order: 'popular',
    page: 1,
    per_page: 10,
    keyword: '',
  }

  constructor(
    private pixabayService: PixabayService,
  ){}

  keySearch(formData: object) {
    this.searching = true;
    this.pixabayService.get(formData)
      .then(
        (response) => {
          this.searching = false;
          let tmp = Object.values(response);
          this.searched = true;
          this.total = tmp[0];
          this.pictureList = tmp[1];
          this.formData = formData;
        }
      )
      .catch(
        (error) => console.log(error)
      );
  }
}
