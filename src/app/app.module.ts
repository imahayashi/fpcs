import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { ModalComponent } from './component/modal/modal.component';

import { LazyLoadImageModule } from 'ng-lazyload-image';

// 追加
import { MatProgressSpinnerModule, MatSpinner } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [
    AppComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    HttpClientModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    RouterModule,
    BrowserModule,
    FormsModule,
    LazyLoadImageModule,
    MatProgressSpinnerModule,
  ],
  bootstrap: [AppComponent],
  providers: [],
  entryComponents: [ModalComponent, MatSpinner]
})
export class AppModule { }

